/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import cl.trello4.model.Board;
import cl.trello4.model.Card;
import cl.trello4j.Trello;
import cl.trello4j.TrelloImpl;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.primefaces.model.DualListModel;

@Named("trelloController")
@SessionScoped
public class TrelloController implements Serializable {

    static Trello trello = new TrelloImpl("d269beaefe036e410f090f10941b5d08", "5eef872dcf4b664c85593f6c7a9327608d8a47754362add1dc4b95ac98645c2b");

    private String board;
    int boardId;
    private String boardSeleccionada;
    boolean render = false;

    public String getBoardSeleccionada() {
        return boardSeleccionada;
    }

    public void setBoardSeleccionada(String boardSeleccionada) {
        this.boardSeleccionada = boardSeleccionada;
    }

    public String getBoard() {
        System.out.println("Hola");
        return board;
    }

    public List<Card> getCards() {
        List<Card> cards = trello.getCardsByBoard(board);
        return cards;
    }

    public void setBoard(String board) {
        System.out.println("CHAO");
        this.board = board;
    }

    public int getBoardId() {
        return boardId;
    }

    public void setBoardId(int boardId) {
        this.boardId = boardId;
    }

    public boolean isRender() {
        return render;
    }
    
    public List<Card> getCardsByBoardId() {
        this.render = false;
        if (this.boardSeleccionada == null) {
            List<Card> lista = null;
            return lista;
        } else {
            this.render = true;
            return trello.getCardsByBoard(this.boardSeleccionada);
        }
    }

    public String hola() {
        List<Board> org = trello.getBoardsByMember("me");
        return trello.getMember("me").getFullName();
    }

    public List<Board> getAllBoards() {
        return trello.getBoardsByMember("me");
    }

    public DualListModel<String> getAllBoardsString() {
        List<String> no_linked = new ArrayList<>();
        List<String> nombres = new ArrayList<>();
        List<Board> boards = trello.getBoardsByMember("me");
        for (int i = 0; i < boards.size(); i++) {
            no_linked.add(boards.get(i).getName());
//            nombres.add(boards.get(i).getName());
        }
        return new DualListModel<String>(no_linked, nombres);
    }
}
