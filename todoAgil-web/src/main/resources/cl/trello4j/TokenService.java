package cl.trello4j;

import cl.trello4.model.Member;
import cl.trello4.model.Token;

/**
 * The Interface TokenService.
 */
public interface TokenService {

	/**
	 * Gets the token.
	 * 
	 * @param tokenId
	 *            the token id
	 * @return the token
	 */
	Token getToken(String tokenId, String... filter);

	/**
	 * Gets the member by token.
	 * 
	 * @param tokenId
	 *            the token id
	 * @return the member by token
	 */
	Member getMemberByToken(String tokenId, String... filter);
}
