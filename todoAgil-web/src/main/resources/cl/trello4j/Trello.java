package cl.trello4j;

import cl.trello4.model.Type;

/**
 * The Interface Trello.
 */
public interface Trello extends OrganizationService, NotificationService,
		BoardService, CardService, ActionService, ListService, MemberService,
		ChecklistService, TokenService {

	/**
	 * Gets the type.
	 * 
	 * @param idOrName
	 *            the id or name
	 * @return the type
	 */
	Type getType(String idOrName);

    public String getStringBoard(final String boardId,
            final String... filter);

}