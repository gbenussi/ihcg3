package cl.trello4j;

import cl.trello4.model.Action;
import cl.trello4.model.Board;
import cl.trello4.model.Card;
import cl.trello4.model.List;
import cl.trello4.model.Member;
import cl.trello4.model.Organization;

/**
 * The Interface ActionService.
 * 
 * @author joel
 */
public interface ActionService {

	Action getAction(String actionId, String... filter);

	Board getBoardByAction(String actionId, String... filter);

	Card getCardByAction(String actionId, String... filter);

	Member getMemberByAction(String actionId, String... filter);

	Member getMemberCreatorByAction(String actionId, String... filter);

	Organization getOrganizationByAction(String actionId, String... filter);

	List getListByAction(String actionId, String... filter);

}