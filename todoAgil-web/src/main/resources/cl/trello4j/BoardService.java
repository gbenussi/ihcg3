package cl.trello4j;

import java.util.List;

import cl.trello4.model.Action;
import cl.trello4.model.Board;
import cl.trello4.model.Board.Prefs;
import cl.trello4.model.Card;
import cl.trello4.model.Checklist;
import cl.trello4.model.Member;
import cl.trello4.model.Organization;

/**
 * 
 * @author joel
 */
public interface BoardService {

	Board getBoard(String boardId);

	List<Action> getActionsByBoard(String boardId, String... filter);

	Organization getOrganizationByBoard(String boardId, String... filter);

	List<Member> getMembersInvitedByBoard(String boardId, String... filter);

	List<Member> getMembersByBoard(String boardId, String... filter);

	List<cl.trello4.model.List> getListByBoard(String boardId,
			String... filter);

	List<Checklist> getChecklistByBoard(String boardId);

	List<Card> getCardsByBoard(String boardId, String... filter);

	Prefs getPrefsByBoard(String boardId);

}