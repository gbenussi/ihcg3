package cl.trello4j;

import java.util.List;
import java.util.Map;

import cl.trello4.model.Action;
import cl.trello4.model.Board;
import cl.trello4.model.Card;
import cl.trello4.model.Card.Attachment;
import cl.trello4.model.Checklist;
import cl.trello4.model.Checklist.CheckItem;
import cl.trello4.model.Member;

/**
 * The Interface CardService.
 * 
 * @author 
 */
public interface CardService {

	Card getCard(String cardId);

	List<Action> getActionsByCard(String cardId);

	List<Attachment> getAttachmentsByCard(String cardId);

	Board getBoardByCard(String cardId, String... filter);

	List<CheckItem> getCheckItemStatesByCard(String cardId);

	List<Checklist> getChecklistByCard(String cardId);

	cl.trello4.model.List getListByCard(String cardId, String... filter);

	List<Member> getMembersByCard(String cardId);

	/**
	 * Add a new {@link Card} with the optional keyValue pairs.
	 * @param idList Id of the {@link cl.trello4.model.List}
	 *               the card should be added to.
	 * @param name Name of the new card.
	 * @param keyValeMap Map of the optional key-value-pairs.
	 */
	Card createCard(String idList, String name, Map<String, String> keyValeMap);
}
