<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" 
      xmlns:h="http://java.sun.com/jsf/html"
      xmlns:f="http://java.sun.com/jsf/core"
      >
    <h:head>
        <title>index.xhtml</title>
    </h:head>

    <h:body>

        <h1>JSF 2.0 View Data From The Database Table Example</h1>

        <h:dataTable value="#{usuarioBean.getUserList()}" var="u" border="1">

            <h:column>
                <f:facet name="header">
                    User ID
                </f:facet>
                #{u.userID}
            </h:column>

            <h:column>
                <f:facet name="header">
                    Name
                </f:facet>
                #{u.name}
            </h:column>

            <h:column>
                <f:facet name="header">
                    Address
                </f:facet>
                #{u.address}
            </h:column>

            <h:column>
                <f:facet name="header">
                    Created Date
                </f:facet>
                #{u.created_date}
            </h:column>

        </h:dataTable>

    </h:body>

</html>