$(function () {

    var onAuthorize = function () {
        updateLoggedIn();
        $("#output").empty();

        Trello.members.get("me", function (member) {
            $("#fullName").text(member.fullName);
            console.log(member);

            var $cards = $("<div>").text("Cargando boards...").appendTo("#output");

            // Output a list of all of the cards that the member 
            // is assigned to
            Trello.get("members/me/boards", function (boards) {
                $cards.empty();
                console.log(boards);
                $.each(boards, function (ix, board) {
                    //$("<a>").attr({href: board.url, target: "trello"}).addClass("card").text(board.name).appendTo($cards);
                    //<button type="button" class="btn btn-primary">Primary</button>
                    $('<p class="bg-primary" style="height: 50px; ">' + board.name + '</p>').appendTo($cards);
                    //$("<a>").attr({href: "board.xhtml", target: "trello"}).addClass("card").text(board.name).appendTo($cards);
//                    Trello.get("members/me/boards/" + board.id, function (boards) {
//                        console.log(boards);
//                    });
                });
            });
        });

    };

    var updateLoggedIn = function () {
        var isLoggedIn = Trello.authorized();
        $("#loggedout").toggle(!isLoggedIn);
        $("#loggedin").toggle(isLoggedIn);
    };

    var logout = function () {
        Trello.deauthorize();
        updateLoggedIn();
    };

    Trello.authorize({
        interactive: false,
        success: onAuthorize
    });

    $("#connectLink").click(function () {
        Trello.authorize({
            type: "popup",
            success: onAuthorize
        });
    });

    $("#disconnect").click(logout);

});