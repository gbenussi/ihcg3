/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.UserRol;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author elias
 */
@Local
public interface UserRolFacadeLocal {

    void create(UserRol userRol);

    void edit(UserRol userRol);

    void remove(UserRol userRol);

    UserRol find(Object id);

    List<UserRol> findAll();

    List<UserRol> findRange(int[] range);

    int count();
    
}
