/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.AsignatureSprint;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author elias
 */
@Local
public interface AsignatureSprintFacadeLocal {

    void create(AsignatureSprint asignatureSprint);

    void edit(AsignatureSprint asignatureSprint);

    void remove(AsignatureSprint asignatureSprint);

    AsignatureSprint find(Object id);

    List<AsignatureSprint> findAll();

    List<AsignatureSprint> findRange(int[] range);

    int count();
    
}
