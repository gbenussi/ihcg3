/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "asignature_sprint")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AsignatureSprint.findAll", query = "SELECT a FROM AsignatureSprint a"),
    @NamedQuery(name = "AsignatureSprint.findByAsignatureSprintId", query = "SELECT a FROM AsignatureSprint a WHERE a.asignatureSprintId = :asignatureSprintId"),
    @NamedQuery(name = "AsignatureSprint.findByAsignatureSprintName", query = "SELECT a FROM AsignatureSprint a WHERE a.asignatureSprintName = :asignatureSprintName"),
    @NamedQuery(name = "AsignatureSprint.findByAsignatureSprintDescription", query = "SELECT a FROM AsignatureSprint a WHERE a.asignatureSprintDescription = :asignatureSprintDescription"),
    @NamedQuery(name = "AsignatureSprint.findByAsignatureSprintStartDate", query = "SELECT a FROM AsignatureSprint a WHERE a.asignatureSprintStartDate = :asignatureSprintStartDate"),
    @NamedQuery(name = "AsignatureSprint.findByAsignatureSprintEndDate", query = "SELECT a FROM AsignatureSprint a WHERE a.asignatureSprintEndDate = :asignatureSprintEndDate")})
public class AsignatureSprint implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "asignature_sprint_id")
    private Integer asignatureSprintId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "asignature_sprint_name")
    private String asignatureSprintName;
    @Size(max = 200)
    @Column(name = "asignature_sprint_description")
    private String asignatureSprintDescription;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignature_sprint_start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date asignatureSprintStartDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignature_sprint_end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date asignatureSprintEndDate;
    @JoinColumn(name = "asignature_id", referencedColumnName = "asignature_id")
    @ManyToOne(optional = false)
    private Asignature asignatureId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "asignatureSprintId")
    private Collection<SprintGroupp> sprintGrouppCollection;

    public AsignatureSprint() {
    }

    public AsignatureSprint(Integer asignatureSprintId) {
        this.asignatureSprintId = asignatureSprintId;
    }

    public AsignatureSprint(Integer asignatureSprintId, String asignatureSprintName, Date asignatureSprintStartDate, Date asignatureSprintEndDate) {
        this.asignatureSprintId = asignatureSprintId;
        this.asignatureSprintName = asignatureSprintName;
        this.asignatureSprintStartDate = asignatureSprintStartDate;
        this.asignatureSprintEndDate = asignatureSprintEndDate;
    }

    public Integer getAsignatureSprintId() {
        return asignatureSprintId;
    }

    public void setAsignatureSprintId(Integer asignatureSprintId) {
        this.asignatureSprintId = asignatureSprintId;
    }

    public String getAsignatureSprintName() {
        return asignatureSprintName;
    }

    public void setAsignatureSprintName(String asignatureSprintName) {
        this.asignatureSprintName = asignatureSprintName;
    }

    public String getAsignatureSprintDescription() {
        return asignatureSprintDescription;
    }

    public void setAsignatureSprintDescription(String asignatureSprintDescription) {
        this.asignatureSprintDescription = asignatureSprintDescription;
    }

    public Date getAsignatureSprintStartDate() {
        return asignatureSprintStartDate;
    }

    public void setAsignatureSprintStartDate(Date asignatureSprintStartDate) {
        this.asignatureSprintStartDate = asignatureSprintStartDate;
    }

    public Date getAsignatureSprintEndDate() {
        return asignatureSprintEndDate;
    }

    public void setAsignatureSprintEndDate(Date asignatureSprintEndDate) {
        this.asignatureSprintEndDate = asignatureSprintEndDate;
    }

    public Asignature getAsignatureId() {
        return asignatureId;
    }

    public void setAsignatureId(Asignature asignatureId) {
        this.asignatureId = asignatureId;
    }

    @XmlTransient
    public Collection<SprintGroupp> getSprintGrouppCollection() {
        return sprintGrouppCollection;
    }

    public void setSprintGrouppCollection(Collection<SprintGroupp> sprintGrouppCollection) {
        this.sprintGrouppCollection = sprintGrouppCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (asignatureSprintId != null ? asignatureSprintId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AsignatureSprint)) {
            return false;
        }
        AsignatureSprint other = (AsignatureSprint) object;
        if ((this.asignatureSprintId == null && other.asignatureSprintId != null) || (this.asignatureSprintId != null && !this.asignatureSprintId.equals(other.asignatureSprintId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.AsignatureSprint[ asignatureSprintId=" + asignatureSprintId + " ]";
    }
    
}
