/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "card")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Card.findAll", query = "SELECT c FROM Card c"),
    @NamedQuery(name = "Card.findByCardId", query = "SELECT c FROM Card c WHERE c.cardId = :cardId"),
    @NamedQuery(name = "Card.findByExtCardId", query = "SELECT c FROM Card c WHERE c.extCardId = :extCardId"),
    @NamedQuery(name = "Card.findByCardName", query = "SELECT c FROM Card c WHERE c.cardName = :cardName"),
    @NamedQuery(name = "Card.findByCardDeadline", query = "SELECT c FROM Card c WHERE c.cardDeadline = :cardDeadline"),
    @NamedQuery(name = "Card.findByCardStartDate", query = "SELECT c FROM Card c WHERE c.cardStartDate = :cardStartDate"),
    @NamedQuery(name = "Card.findByCardEndDate", query = "SELECT c FROM Card c WHERE c.cardEndDate = :cardEndDate"),
    @NamedQuery(name = "Card.findByCardCreationDate", query = "SELECT c FROM Card c WHERE c.cardCreationDate = :cardCreationDate")})
public class Card implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "card_id")
    private Integer cardId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ext_card_id")
    private String extCardId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "card_name")
    private String cardName;
    @Column(name = "card_deadline")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardDeadline;
    @Column(name = "card_start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardStartDate;
    @Column(name = "card_end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardEndDate;
    @Column(name = "card_creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cardCreationDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cardId")
    private Collection<UserCardDetail> userCardDetailCollection;
    @JoinColumn(name = "dash_id", referencedColumnName = "dash_id")
    @ManyToOne(optional = false)
    private Dash dashId;
    @JoinColumn(name = "card_status_id", referencedColumnName = "card_status_id")
    @ManyToOne(optional = false)
    private CardStatus cardStatusId;
    @JoinColumn(name = "list_id", referencedColumnName = "list_id")
    @ManyToOne(optional = false)
    private List listId;
    @OneToMany(mappedBy = "cardId")
    private Collection<Activity> activityCollection;

    public Card() {
    }

    public Card(Integer cardId) {
        this.cardId = cardId;
    }

    public Card(Integer cardId, String extCardId, String cardName) {
        this.cardId = cardId;
        this.extCardId = extCardId;
        this.cardName = cardName;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public String getExtCardId() {
        return extCardId;
    }

    public void setExtCardId(String extCardId) {
        this.extCardId = extCardId;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public Date getCardDeadline() {
        return cardDeadline;
    }

    public void setCardDeadline(Date cardDeadline) {
        this.cardDeadline = cardDeadline;
    }

    public Date getCardStartDate() {
        return cardStartDate;
    }

    public void setCardStartDate(Date cardStartDate) {
        this.cardStartDate = cardStartDate;
    }

    public Date getCardEndDate() {
        return cardEndDate;
    }

    public void setCardEndDate(Date cardEndDate) {
        this.cardEndDate = cardEndDate;
    }

    public Date getCardCreationDate() {
        return cardCreationDate;
    }

    public void setCardCreationDate(Date cardCreationDate) {
        this.cardCreationDate = cardCreationDate;
    }

    @XmlTransient
    public Collection<UserCardDetail> getUserCardDetailCollection() {
        return userCardDetailCollection;
    }

    public void setUserCardDetailCollection(Collection<UserCardDetail> userCardDetailCollection) {
        this.userCardDetailCollection = userCardDetailCollection;
    }

    public Dash getDashId() {
        return dashId;
    }

    public void setDashId(Dash dashId) {
        this.dashId = dashId;
    }

    public CardStatus getCardStatusId() {
        return cardStatusId;
    }

    public void setCardStatusId(CardStatus cardStatusId) {
        this.cardStatusId = cardStatusId;
    }

    public List getListId() {
        return listId;
    }

    public void setListId(List listId) {
        this.listId = listId;
    }

    @XmlTransient
    public Collection<Activity> getActivityCollection() {
        return activityCollection;
    }

    public void setActivityCollection(Collection<Activity> activityCollection) {
        this.activityCollection = activityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardId != null ? cardId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Card)) {
            return false;
        }
        Card other = (Card) object;
        if ((this.cardId == null && other.cardId != null) || (this.cardId != null && !this.cardId.equals(other.cardId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.Card[ cardId=" + cardId + " ]";
    }
    
}
