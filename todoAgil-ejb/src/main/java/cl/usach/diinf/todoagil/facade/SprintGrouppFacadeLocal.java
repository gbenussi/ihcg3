/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.SprintGroupp;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author elias
 */
@Local
public interface SprintGrouppFacadeLocal {

    void create(SprintGroupp sprintGroupp);

    void edit(SprintGroupp sprintGroupp);

    void remove(SprintGroupp sprintGroupp);

    SprintGroupp find(Object id);

    List<SprintGroupp> findAll();

    List<SprintGroupp> findRange(int[] range);

    int count();
    
}
