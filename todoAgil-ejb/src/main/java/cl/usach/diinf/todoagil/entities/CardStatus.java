/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "card_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CardStatus.findAll", query = "SELECT c FROM CardStatus c"),
    @NamedQuery(name = "CardStatus.findByCardStatusId", query = "SELECT c FROM CardStatus c WHERE c.cardStatusId = :cardStatusId"),
    @NamedQuery(name = "CardStatus.findByCardStatusName", query = "SELECT c FROM CardStatus c WHERE c.cardStatusName = :cardStatusName")})
public class CardStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "card_status_id")
    private Integer cardStatusId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "card_status_name")
    private String cardStatusName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cardStatusId")
    private Collection<Card> cardCollection;

    public CardStatus() {
    }

    public CardStatus(Integer cardStatusId) {
        this.cardStatusId = cardStatusId;
    }

    public CardStatus(Integer cardStatusId, String cardStatusName) {
        this.cardStatusId = cardStatusId;
        this.cardStatusName = cardStatusName;
    }

    public Integer getCardStatusId() {
        return cardStatusId;
    }

    public void setCardStatusId(Integer cardStatusId) {
        this.cardStatusId = cardStatusId;
    }

    public String getCardStatusName() {
        return cardStatusName;
    }

    public void setCardStatusName(String cardStatusName) {
        this.cardStatusName = cardStatusName;
    }

    @XmlTransient
    public Collection<Card> getCardCollection() {
        return cardCollection;
    }

    public void setCardCollection(Collection<Card> cardCollection) {
        this.cardCollection = cardCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardStatusId != null ? cardStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CardStatus)) {
            return false;
        }
        CardStatus other = (CardStatus) object;
        if ((this.cardStatusId == null && other.cardStatusId != null) || (this.cardStatusId != null && !this.cardStatusId.equals(other.cardStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.CardStatus[ cardStatusId=" + cardStatusId + " ]";
    }
    
}
