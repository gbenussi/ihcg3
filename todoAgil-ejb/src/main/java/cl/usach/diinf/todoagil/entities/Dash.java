/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "dash")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dash.findAll", query = "SELECT d FROM Dash d"),
    @NamedQuery(name = "Dash.findByDashId", query = "SELECT d FROM Dash d WHERE d.dashId = :dashId"),
    @NamedQuery(name = "Dash.findByExternalDashId", query = "SELECT d FROM Dash d WHERE d.externalDashId = :externalDashId"),
    @NamedQuery(name = "Dash.findByDashName", query = "SELECT d FROM Dash d WHERE d.dashName = :dashName"),
    @NamedQuery(name = "Dash.findByDashUrl", query = "SELECT d FROM Dash d WHERE d.dashUrl = :dashUrl")})
public class Dash implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dash_id")
    private Integer dashId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "external_dash_id")
    private String externalDashId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "dash_name")
    private String dashName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "dash_url")
    private String dashUrl;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dashId")
    private Collection<Member1> member1Collection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dashId")
    private Collection<List> listCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dashId")
    private Collection<Team> teamCollection;
    @JoinColumn(name = "sprint_groupp_id", referencedColumnName = "sprint_groupp_id")
    @ManyToOne
    private SprintGroupp sprintGrouppId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dashId")
    private Collection<Card> cardCollection;

    public Dash() {
    }

    public Dash(Integer dashId) {
        this.dashId = dashId;
    }

    public Dash(Integer dashId, String externalDashId, String dashName, String dashUrl) {
        this.dashId = dashId;
        this.externalDashId = externalDashId;
        this.dashName = dashName;
        this.dashUrl = dashUrl;
    }

    public Integer getDashId() {
        return dashId;
    }

    public void setDashId(Integer dashId) {
        this.dashId = dashId;
    }

    public String getExternalDashId() {
        return externalDashId;
    }

    public void setExternalDashId(String externalDashId) {
        this.externalDashId = externalDashId;
    }

    public String getDashName() {
        return dashName;
    }

    public void setDashName(String dashName) {
        this.dashName = dashName;
    }

    public String getDashUrl() {
        return dashUrl;
    }

    public void setDashUrl(String dashUrl) {
        this.dashUrl = dashUrl;
    }

    @XmlTransient
    public Collection<Member1> getMember1Collection() {
        return member1Collection;
    }

    public void setMember1Collection(Collection<Member1> member1Collection) {
        this.member1Collection = member1Collection;
    }

    @XmlTransient
    public Collection<List> getListCollection() {
        return listCollection;
    }

    public void setListCollection(Collection<List> listCollection) {
        this.listCollection = listCollection;
    }

    @XmlTransient
    public Collection<Team> getTeamCollection() {
        return teamCollection;
    }

    public void setTeamCollection(Collection<Team> teamCollection) {
        this.teamCollection = teamCollection;
    }

    public SprintGroupp getSprintGrouppId() {
        return sprintGrouppId;
    }

    public void setSprintGrouppId(SprintGroupp sprintGrouppId) {
        this.sprintGrouppId = sprintGrouppId;
    }

    @XmlTransient
    public Collection<Card> getCardCollection() {
        return cardCollection;
    }

    public void setCardCollection(Collection<Card> cardCollection) {
        this.cardCollection = cardCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dashId != null ? dashId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dash)) {
            return false;
        }
        Dash other = (Dash) object;
        if ((this.dashId == null && other.dashId != null) || (this.dashId != null && !this.dashId.equals(other.dashId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.Dash[ dashId=" + dashId + " ]";
    }
    
}
