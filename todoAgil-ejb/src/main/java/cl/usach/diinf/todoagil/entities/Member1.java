/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "member")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Member1.findAll", query = "SELECT m FROM Member1 m"),
    @NamedQuery(name = "Member1.findByMemberId", query = "SELECT m FROM Member1 m WHERE m.memberId = :memberId"),
    @NamedQuery(name = "Member1.findByExternalMemberId", query = "SELECT m FROM Member1 m WHERE m.externalMemberId = :externalMemberId"),
    @NamedQuery(name = "Member1.findByUserMemberName", query = "SELECT m FROM Member1 m WHERE m.userMemberName = :userMemberName")})
public class Member1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "member_id")
    private Integer memberId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "external_member_id")
    private String externalMemberId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "user_member_name")
    private String userMemberName;
    @JoinColumn(name = "account_id", referencedColumnName = "account_id")
    @ManyToOne
    private Account accountId;
    @JoinColumn(name = "dash_id", referencedColumnName = "dash_id")
    @ManyToOne(optional = false)
    private Dash dashId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "memberId")
    private Collection<UserCardDetail> userCardDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "memberId")
    private Collection<Activity> activityCollection;

    public Member1() {
    }

    public Member1(Integer memberId) {
        this.memberId = memberId;
    }

    public Member1(Integer memberId, String externalMemberId, String userMemberName) {
        this.memberId = memberId;
        this.externalMemberId = externalMemberId;
        this.userMemberName = userMemberName;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getExternalMemberId() {
        return externalMemberId;
    }

    public void setExternalMemberId(String externalMemberId) {
        this.externalMemberId = externalMemberId;
    }

    public String getUserMemberName() {
        return userMemberName;
    }

    public void setUserMemberName(String userMemberName) {
        this.userMemberName = userMemberName;
    }

    public Account getAccountId() {
        return accountId;
    }

    public void setAccountId(Account accountId) {
        this.accountId = accountId;
    }

    public Dash getDashId() {
        return dashId;
    }

    public void setDashId(Dash dashId) {
        this.dashId = dashId;
    }

    @XmlTransient
    public Collection<UserCardDetail> getUserCardDetailCollection() {
        return userCardDetailCollection;
    }

    public void setUserCardDetailCollection(Collection<UserCardDetail> userCardDetailCollection) {
        this.userCardDetailCollection = userCardDetailCollection;
    }

    @XmlTransient
    public Collection<Activity> getActivityCollection() {
        return activityCollection;
    }

    public void setActivityCollection(Collection<Activity> activityCollection) {
        this.activityCollection = activityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (memberId != null ? memberId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Member1)) {
            return false;
        }
        Member1 other = (Member1) object;
        if ((this.memberId == null && other.memberId != null) || (this.memberId != null && !this.memberId.equals(other.memberId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.Member1[ memberId=" + memberId + " ]";
    }
    
}
