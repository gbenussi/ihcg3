/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "user_card_detail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserCardDetail.findAll", query = "SELECT u FROM UserCardDetail u"),
    @NamedQuery(name = "UserCardDetail.findByUserCardDetailId", query = "SELECT u FROM UserCardDetail u WHERE u.userCardDetailId = :userCardDetailId")})
public class UserCardDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_card_detail_id")
    private Integer userCardDetailId;
    @JoinColumn(name = "member_id", referencedColumnName = "member_id")
    @ManyToOne(optional = false)
    private Member1 memberId;
    @JoinColumn(name = "card_id", referencedColumnName = "card_id")
    @ManyToOne(optional = false)
    private Card cardId;

    public UserCardDetail() {
    }

    public UserCardDetail(Integer userCardDetailId) {
        this.userCardDetailId = userCardDetailId;
    }

    public Integer getUserCardDetailId() {
        return userCardDetailId;
    }

    public void setUserCardDetailId(Integer userCardDetailId) {
        this.userCardDetailId = userCardDetailId;
    }

    public Member1 getMemberId() {
        return memberId;
    }

    public void setMemberId(Member1 memberId) {
        this.memberId = memberId;
    }

    public Card getCardId() {
        return cardId;
    }

    public void setCardId(Card cardId) {
        this.cardId = cardId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userCardDetailId != null ? userCardDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserCardDetail)) {
            return false;
        }
        UserCardDetail other = (UserCardDetail) object;
        if ((this.userCardDetailId == null && other.userCardDetailId != null) || (this.userCardDetailId != null && !this.userCardDetailId.equals(other.userCardDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.UserCardDetail[ userCardDetailId=" + userCardDetailId + " ]";
    }
    
}
