/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "list")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "List.findAll", query = "SELECT l FROM List l"),
    @NamedQuery(name = "List.findByListId", query = "SELECT l FROM List l WHERE l.listId = :listId"),
    @NamedQuery(name = "List.findByExtListId", query = "SELECT l FROM List l WHERE l.extListId = :extListId"),
    @NamedQuery(name = "List.findByListName", query = "SELECT l FROM List l WHERE l.listName = :listName"),
    @NamedQuery(name = "List.findByPosition", query = "SELECT l FROM List l WHERE l.position = :position")})
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "list_id")
    private Integer listId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ext_list_id")
    private String extListId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "list_name")
    private String listName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "position")
    private int position;
    @JoinColumn(name = "dash_id", referencedColumnName = "dash_id")
    @ManyToOne(optional = false)
    private Dash dashId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "listId")
    private Collection<Card> cardCollection;

    public List() {
    }

    public List(Integer listId) {
        this.listId = listId;
    }

    public List(Integer listId, String extListId, String listName, int position) {
        this.listId = listId;
        this.extListId = extListId;
        this.listName = listName;
        this.position = position;
    }

    public Integer getListId() {
        return listId;
    }

    public void setListId(Integer listId) {
        this.listId = listId;
    }

    public String getExtListId() {
        return extListId;
    }

    public void setExtListId(String extListId) {
        this.extListId = extListId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Dash getDashId() {
        return dashId;
    }

    public void setDashId(Dash dashId) {
        this.dashId = dashId;
    }

    @XmlTransient
    public Collection<Card> getCardCollection() {
        return cardCollection;
    }

    public void setCardCollection(Collection<Card> cardCollection) {
        this.cardCollection = cardCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (listId != null ? listId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof List)) {
            return false;
        }
        List other = (List) object;
        if ((this.listId == null && other.listId != null) || (this.listId != null && !this.listId.equals(other.listId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.List[ listId=" + listId + " ]";
    }
    
}
