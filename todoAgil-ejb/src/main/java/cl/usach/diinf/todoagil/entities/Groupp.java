/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "groupp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Groupp.findAll", query = "SELECT g FROM Groupp g"),
    @NamedQuery(name = "Groupp.findByIdGroupp", query = "SELECT g FROM Groupp g WHERE g.idGroupp = :idGroupp"),
    @NamedQuery(name = "Groupp.findByGrouppUser", query = "SELECT g FROM Groupp g WHERE g.grouppUser = :grouppUser"),
    @NamedQuery(name = "Groupp.findByGrouppName", query = "SELECT g FROM Groupp g WHERE g.grouppName = :grouppName")})
public class Groupp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_groupp")
    private Integer idGroupp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "groupp_user")
    private String grouppUser;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "groupp_name")
    private String grouppName;

    public Groupp() {
    }

    public Groupp(Integer idGroupp) {
        this.idGroupp = idGroupp;
    }

    public Groupp(Integer idGroupp, String grouppUser, String grouppName) {
        this.idGroupp = idGroupp;
        this.grouppUser = grouppUser;
        this.grouppName = grouppName;
    }

    public Integer getIdGroupp() {
        return idGroupp;
    }

    public void setIdGroupp(Integer idGroupp) {
        this.idGroupp = idGroupp;
    }

    public String getGrouppUser() {
        return grouppUser;
    }

    public void setGrouppUser(String grouppUser) {
        this.grouppUser = grouppUser;
    }

    public String getGrouppName() {
        return grouppName;
    }

    public void setGrouppName(String grouppName) {
        this.grouppName = grouppName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGroupp != null ? idGroupp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Groupp)) {
            return false;
        }
        Groupp other = (Groupp) object;
        if ((this.idGroupp == null && other.idGroupp != null) || (this.idGroupp != null && !this.idGroupp.equals(other.idGroupp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.Groupp[ idGroupp=" + idGroupp + " ]";
    }
    
}
