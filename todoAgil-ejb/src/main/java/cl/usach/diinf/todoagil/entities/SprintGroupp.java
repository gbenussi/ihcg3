/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "sprint_groupp")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SprintGroupp.findAll", query = "SELECT s FROM SprintGroupp s"),
    @NamedQuery(name = "SprintGroupp.findBySprintGrouppId", query = "SELECT s FROM SprintGroupp s WHERE s.sprintGrouppId = :sprintGrouppId"),
    @NamedQuery(name = "SprintGroupp.findBySprintGrouppName", query = "SELECT s FROM SprintGroupp s WHERE s.sprintGrouppName = :sprintGrouppName"),
    @NamedQuery(name = "SprintGroupp.findBySprintGrouppTechnicalObjectives", query = "SELECT s FROM SprintGroupp s WHERE s.sprintGrouppTechnicalObjectives = :sprintGrouppTechnicalObjectives"),
    @NamedQuery(name = "SprintGroupp.findBySprintGrouppUserObjectives", query = "SELECT s FROM SprintGroupp s WHERE s.sprintGrouppUserObjectives = :sprintGrouppUserObjectives")})
public class SprintGroupp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sprint_groupp_id")
    private Integer sprintGrouppId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "sprint_groupp_name")
    private String sprintGrouppName;
    @Size(max = 100)
    @Column(name = "sprint_groupp_technical_objectives")
    private String sprintGrouppTechnicalObjectives;
    @Size(max = 100)
    @Column(name = "sprint_groupp_user_objectives")
    private String sprintGrouppUserObjectives;
    @OneToMany(mappedBy = "sprintGrouppId")
    private Collection<Dash> dashCollection;
    @JoinColumn(name = "asignature_sprint_id", referencedColumnName = "asignature_sprint_id")
    @ManyToOne(optional = false)
    private AsignatureSprint asignatureSprintId;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    private User userId;

    public SprintGroupp() {
    }

    public SprintGroupp(Integer sprintGrouppId) {
        this.sprintGrouppId = sprintGrouppId;
    }

    public SprintGroupp(Integer sprintGrouppId, String sprintGrouppName) {
        this.sprintGrouppId = sprintGrouppId;
        this.sprintGrouppName = sprintGrouppName;
    }

    public Integer getSprintGrouppId() {
        return sprintGrouppId;
    }

    public void setSprintGrouppId(Integer sprintGrouppId) {
        this.sprintGrouppId = sprintGrouppId;
    }

    public String getSprintGrouppName() {
        return sprintGrouppName;
    }

    public void setSprintGrouppName(String sprintGrouppName) {
        this.sprintGrouppName = sprintGrouppName;
    }

    public String getSprintGrouppTechnicalObjectives() {
        return sprintGrouppTechnicalObjectives;
    }

    public void setSprintGrouppTechnicalObjectives(String sprintGrouppTechnicalObjectives) {
        this.sprintGrouppTechnicalObjectives = sprintGrouppTechnicalObjectives;
    }

    public String getSprintGrouppUserObjectives() {
        return sprintGrouppUserObjectives;
    }

    public void setSprintGrouppUserObjectives(String sprintGrouppUserObjectives) {
        this.sprintGrouppUserObjectives = sprintGrouppUserObjectives;
    }

    @XmlTransient
    public Collection<Dash> getDashCollection() {
        return dashCollection;
    }

    public void setDashCollection(Collection<Dash> dashCollection) {
        this.dashCollection = dashCollection;
    }

    public AsignatureSprint getAsignatureSprintId() {
        return asignatureSprintId;
    }

    public void setAsignatureSprintId(AsignatureSprint asignatureSprintId) {
        this.asignatureSprintId = asignatureSprintId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sprintGrouppId != null ? sprintGrouppId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SprintGroupp)) {
            return false;
        }
        SprintGroupp other = (SprintGroupp) object;
        if ((this.sprintGrouppId == null && other.sprintGrouppId != null) || (this.sprintGrouppId != null && !this.sprintGrouppId.equals(other.sprintGrouppId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.SprintGroupp[ sprintGrouppId=" + sprintGrouppId + " ]";
    }
    
}
