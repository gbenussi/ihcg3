/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.ActivityType;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author elias
 */
@Stateless
public class ActivityTypeFacade extends AbstractFacade<ActivityType> implements ActivityTypeFacadeLocal {
    @PersistenceContext(unitName = "todoAgil-ejb1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ActivityTypeFacade() {
        super(ActivityType.class);
    }
    
}
