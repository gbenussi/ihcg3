/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.ActivityType;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author elias
 */
@Local
public interface ActivityTypeFacadeLocal {

    void create(ActivityType activityType);

    void edit(ActivityType activityType);

    void remove(ActivityType activityType);

    ActivityType find(Object id);

    List<ActivityType> findAll();

    List<ActivityType> findRange(int[] range);

    int count();
    
}
