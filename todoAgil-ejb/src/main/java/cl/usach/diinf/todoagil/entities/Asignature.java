/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "asignature")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asignature.findAll", query = "SELECT a FROM Asignature a"),
    @NamedQuery(name = "Asignature.findByAsignatureId", query = "SELECT a FROM Asignature a WHERE a.asignatureId = :asignatureId"),
    @NamedQuery(name = "Asignature.findByAsignatureName", query = "SELECT a FROM Asignature a WHERE a.asignatureName = :asignatureName"),
    @NamedQuery(name = "Asignature.findByAsignatureTheoryHours", query = "SELECT a FROM Asignature a WHERE a.asignatureTheoryHours = :asignatureTheoryHours"),
    @NamedQuery(name = "Asignature.findByAsignatureExercisesHours", query = "SELECT a FROM Asignature a WHERE a.asignatureExercisesHours = :asignatureExercisesHours"),
    @NamedQuery(name = "Asignature.findByAsignatureLabHours", query = "SELECT a FROM Asignature a WHERE a.asignatureLabHours = :asignatureLabHours"),
    @NamedQuery(name = "Asignature.findByAsignatureSemester", query = "SELECT a FROM Asignature a WHERE a.asignatureSemester = :asignatureSemester"),
    @NamedQuery(name = "Asignature.findByAsignatureYear", query = "SELECT a FROM Asignature a WHERE a.asignatureYear = :asignatureYear"),
    @NamedQuery(name = "Asignature.findByAsignatureClose", query = "SELECT a FROM Asignature a WHERE a.asignatureClose = :asignatureClose")})
public class Asignature implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "asignature_id")
    private Integer asignatureId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "asignature_name")
    private String asignatureName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignature_theory_hours")
    private int asignatureTheoryHours;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignature_exercises_hours")
    private int asignatureExercisesHours;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignature_lab_hours")
    private int asignatureLabHours;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignature_semester")
    private int asignatureSemester;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignature_year")
    private int asignatureYear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "asignature_close")
    private int asignatureClose;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "asignatureId")
    private Collection<AsignatureSprint> asignatureSprintCollection;
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    private User userId;

    public Asignature() {
    }

    public Asignature(Integer asignatureId) {
        this.asignatureId = asignatureId;
    }

    public Asignature(Integer asignatureId, String asignatureName, int asignatureTheoryHours, int asignatureExercisesHours, int asignatureLabHours, int asignatureSemester, int asignatureYear, int asignatureClose) {
        this.asignatureId = asignatureId;
        this.asignatureName = asignatureName;
        this.asignatureTheoryHours = asignatureTheoryHours;
        this.asignatureExercisesHours = asignatureExercisesHours;
        this.asignatureLabHours = asignatureLabHours;
        this.asignatureSemester = asignatureSemester;
        this.asignatureYear = asignatureYear;
        this.asignatureClose = asignatureClose;
    }

    public Integer getAsignatureId() {
        return asignatureId;
    }

    public void setAsignatureId(Integer asignatureId) {
        this.asignatureId = asignatureId;
    }

    public String getAsignatureName() {
        return asignatureName;
    }

    public void setAsignatureName(String asignatureName) {
        this.asignatureName = asignatureName;
    }

    public int getAsignatureTheoryHours() {
        return asignatureTheoryHours;
    }

    public void setAsignatureTheoryHours(int asignatureTheoryHours) {
        this.asignatureTheoryHours = asignatureTheoryHours;
    }

    public int getAsignatureExercisesHours() {
        return asignatureExercisesHours;
    }

    public void setAsignatureExercisesHours(int asignatureExercisesHours) {
        this.asignatureExercisesHours = asignatureExercisesHours;
    }

    public int getAsignatureLabHours() {
        return asignatureLabHours;
    }

    public void setAsignatureLabHours(int asignatureLabHours) {
        this.asignatureLabHours = asignatureLabHours;
    }

    public int getAsignatureSemester() {
        return asignatureSemester;
    }

    public void setAsignatureSemester(int asignatureSemester) {
        this.asignatureSemester = asignatureSemester;
    }

    public int getAsignatureYear() {
        return asignatureYear;
    }

    public void setAsignatureYear(int asignatureYear) {
        this.asignatureYear = asignatureYear;
    }

    public int getAsignatureClose() {
        return asignatureClose;
    }

    public void setAsignatureClose(int asignatureClose) {
        this.asignatureClose = asignatureClose;
    }

    @XmlTransient
    public Collection<AsignatureSprint> getAsignatureSprintCollection() {
        return asignatureSprintCollection;
    }

    public void setAsignatureSprintCollection(Collection<AsignatureSprint> asignatureSprintCollection) {
        this.asignatureSprintCollection = asignatureSprintCollection;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (asignatureId != null ? asignatureId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asignature)) {
            return false;
        }
        Asignature other = (Asignature) object;
        if ((this.asignatureId == null && other.asignatureId != null) || (this.asignatureId != null && !this.asignatureId.equals(other.asignatureId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.Asignature[ asignatureId=" + asignatureId + " ]";
    }
    
}
