/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "user_rol")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserRol.findAll", query = "SELECT u FROM UserRol u"),
    @NamedQuery(name = "UserRol.findByUserRolId", query = "SELECT u FROM UserRol u WHERE u.userRolId = :userRolId"),
    @NamedQuery(name = "UserRol.findByUserRolName", query = "SELECT u FROM UserRol u WHERE u.userRolName = :userRolName"),
    @NamedQuery(name = "UserRol.findByUserRolPermission", query = "SELECT u FROM UserRol u WHERE u.userRolPermission = :userRolPermission")})
public class UserRol implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_rol_id")
    private Integer userRolId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "user_rol_name")
    private String userRolName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "user_rol_permission")
    private String userRolPermission;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userRolId")
    private Collection<User> userCollection;

    public UserRol() {
    }

    public UserRol(Integer userRolId) {
        this.userRolId = userRolId;
    }

    public UserRol(Integer userRolId, String userRolName, String userRolPermission) {
        this.userRolId = userRolId;
        this.userRolName = userRolName;
        this.userRolPermission = userRolPermission;
    }

    public Integer getUserRolId() {
        return userRolId;
    }

    public void setUserRolId(Integer userRolId) {
        this.userRolId = userRolId;
    }

    public String getUserRolName() {
        return userRolName;
    }

    public void setUserRolName(String userRolName) {
        this.userRolName = userRolName;
    }

    public String getUserRolPermission() {
        return userRolPermission;
    }

    public void setUserRolPermission(String userRolPermission) {
        this.userRolPermission = userRolPermission;
    }

    @XmlTransient
    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userRolId != null ? userRolId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserRol)) {
            return false;
        }
        UserRol other = (UserRol) object;
        if ((this.userRolId == null && other.userRolId != null) || (this.userRolId != null && !this.userRolId.equals(other.userRolId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.usach.diinf.todoagil.entities.UserRol[ userRolId=" + userRolId + " ]";
    }
    
}
