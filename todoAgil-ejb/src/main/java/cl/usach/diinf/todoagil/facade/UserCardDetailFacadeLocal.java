/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.UserCardDetail;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author elias
 */
@Local
public interface UserCardDetailFacadeLocal {

    void create(UserCardDetail userCardDetail);

    void edit(UserCardDetail userCardDetail);

    void remove(UserCardDetail userCardDetail);

    UserCardDetail find(Object id);

    List<UserCardDetail> findAll();

    List<UserCardDetail> findRange(int[] range);

    int count();
    
}
