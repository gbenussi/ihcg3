/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.CardStatus;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author elias
 */
@Local
public interface CardStatusFacadeLocal {

    void create(CardStatus cardStatus);

    void edit(CardStatus cardStatus);

    void remove(CardStatus cardStatus);

    CardStatus find(Object id);

    List<CardStatus> findAll();

    List<CardStatus> findRange(int[] range);

    int count();
    
}
