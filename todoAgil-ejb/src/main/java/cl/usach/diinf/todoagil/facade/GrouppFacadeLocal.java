/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.Groupp;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author elias
 */
@Local
public interface GrouppFacadeLocal {

    void create(Groupp groupp);

    void edit(Groupp groupp);

    void remove(Groupp groupp);

    Groupp find(Object id);

    List<Groupp> findAll();

    List<Groupp> findRange(int[] range);

    int count();
    
}
