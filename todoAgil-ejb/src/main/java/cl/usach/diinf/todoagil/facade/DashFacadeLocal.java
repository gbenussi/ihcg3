/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.Dash;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author elias
 */
@Local
public interface DashFacadeLocal {

    void create(Dash dash);

    void edit(Dash dash);

    void remove(Dash dash);

    Dash find(Object id);

    List<Dash> findAll();

    List<Dash> findRange(int[] range);

    int count();
    
}
