/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usach.diinf.todoagil.facade;

import cl.usach.diinf.todoagil.entities.Asignature;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author elias
 */
@Local
public interface AsignatureFacadeLocal {

    void create(Asignature asignature);

    void edit(Asignature asignature);

    void remove(Asignature asignature);

    Asignature find(Object id);

    List<Asignature> findAll();

    List<Asignature> findRange(int[] range);

    int count();
    
}
